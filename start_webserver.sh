#!/bin/bash
docker run \
  --detach \
  --rm \
  --name example_webserver \
  --publish 8080:80 \
  --link example_mysql:db \
  example_webserver_image
