#!/bin/bash
docker run \
  --detach \
  --rm \
  --name example_mysql \
  --volume $(pwd)/data:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=kd \
  --publish 3306:3306 \
  mysql:8.0
