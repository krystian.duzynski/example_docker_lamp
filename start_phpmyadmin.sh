#!/bin/bash
docker run \
  --detach \
  --rm \
  --name example_phpmyadmin \
  --link example_mysql:db \
  -e MYSQL_ROOT_PASSWORD=kd \
  --publish 8081:80 \
  phpmyadmin:5-apache
